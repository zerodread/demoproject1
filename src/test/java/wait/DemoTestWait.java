package wait;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class DemoTestWait extends BaseTest{

    @Test
    public void testImplicitlyWait(){
        driver.navigate().to("https://alfabank.kz");

        WebElement openCreditElement = driver.findElement(By.cssSelector("a#main-slider-get-online-credit-id")); // поиск веб-элемента по структурному локатору cssSelector
        openCreditElement.click();
    }


    @Test
    public void testExplicitlyWait(){
        driver.navigate().to("https://alfabank.kz");

        WebElement openCreditElement = driver.findElement(By.cssSelector("a#main-slider-get-online-credit-id")); // поиск веб-элемента по структурному локатору cssSelector
        waitUntilElementToBeClickable(openCreditElement, 30);
        openCreditElement.click();
    }
}
