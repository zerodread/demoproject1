package wait;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    static WebDriver driver;


    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/test/java/resource/chromedriver.exe"); // указан относительный путь к драйверу
        driver = new ChromeDriver();

        driver.manage().window().maximize();  //развернуть окно

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterTest
    public void tearDown(){
        driver.close();
        driver.quit();
    }

    public void isElementPresented(By locator){
        try {
            driver.findElement(locator);
        }catch (NoSuchElementException ex){
            System.out.println(ex);
        }
    }

    //   Явные ожидания
    //Ожидание - пока элемент станет кликабельным
    public void waitUntilElementToBeClickable(WebElement element, int sec){
        new WebDriverWait(driver, sec).until(ExpectedConditions.elementToBeClickable(element));
    }

    //Ожидание - пока элемент станет видимым
    public void waitUntilElementToBeVisible(WebElement element, int sec){
        new WebDriverWait(driver, sec).until(ExpectedConditions.visibilityOf(element));
    }

    //Ожидание - пока элемент выберится
    public void waitUntilElementToBeSelected(WebElement element, int sec){
        new WebDriverWait(driver, sec).until(ExpectedConditions.elementToBeSelected(element));
    }



}
