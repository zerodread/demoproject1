package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class DemoTest1 {
    private static WebDriver driver; //объявление драйвера на уровне класса (драйвер управляет браузером)

    /*
    * метод будет выполняться перед запуском метода test1()
    * public - модификатор доступа, открыт для всех
    * void метод ничего не возвращает
    * @BeforeTest - аннотация тестового фреймворка Testng
    * */
    @BeforeTest
    public void setUp(){
        /*
        * Как следует из названия, метод setProperty позволяет устанавливать свойства для желаемого браузера,
        * который будет использоваться при автоматизации тестирования.
        * Метод setProperty имеет два атрибута - «propertyName» и «value». PropertyName представляет имя драйвера для конкретного браузера,
        * а значение указывает путь к этому драйверу браузера.
        * */
        System.setProperty("webdriver.chrome.driver", "src/test/java/resource/chromedriver.exe"); // указан относительный путь к драйверу
        driver = new ChromeDriver(); //создаем объект хромдрайвера

        driver.manage().window().maximize();  //развернуть окно
        driver.navigate().to("https://alfabank.kz"); //переход на страницу по адресу, вызов методов navigate().to() идентично методу get()

        /*
        * Неявное ожидание в Selenium используется, чтобы указать веб-драйверу подождать определенное время,
        * прежде чем он выдаст исключение «NoSuchElementException».
        * Значение по умолчанию - 0.
        * После того, как мы установим время, веб-драйвер будет ждать элемента в течение этого времени, прежде чем генерировать исключение.
        * */
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void test1(){
        WebElement openCreditElement = driver.findElement(By.cssSelector("a#main-slider-get-online-credit-id")); // поиск веб-элемента по структурному локатору cssSelector
        openCreditElement.click();
        Assert.assertEquals(driver.getCurrentUrl(), "https://alfabank.kz/persons/credits/e-pil/"); //Assertion - проверяется ожидаемый и фактический результат
    }


    /*
     * метод будет выполняться после метода test1()
     * @AfterTest - аннотация тестового фреймворка Testng
     * */
    @AfterTest
    public void tearDown(){
        driver.close();
        driver.quit();
    }
}
