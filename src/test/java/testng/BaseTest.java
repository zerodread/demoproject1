package testng;

import org.testng.annotations.*;

public class BaseTest {

    @BeforeSuite
    void aStructure(){
        System.out.println("+- before suite/\n" +
                "   +- before group/\n" +
                "      +- before test/\n" +
                "         +- before class/\n" +
                "            +- before method/\n" +
                "               +- test/\n" +
                "            +- after method/\n" +
                "            ...\n" +
                "         +- after class/\n" +
                "         ...\n" +
                "      +- after test/\n" +
                "      ...\n" +
                "   +- after group/\n" +
                "   ...\n" +
                "+- after suite/\n");
    }

    @BeforeSuite
    void beforeTestSuite(){
        System.out.println("@BeforeSuite - Выполняюсь перед Suite");
    }

    @BeforeTest
    void beforeTest(){
        System.out.println("@BeforeTest - Выполняюсь перед Test");
    }

    @BeforeClass
    void beforeClass(){
        System.out.println("@BeforeClass - Выполняюсь перед тестовым классом Test1/Test2");
    }

    @BeforeMethod
    void beforeMethod(){
        System.out.println("@BeforeMethod - Выполняюсь перед тестовым метода test1()/test2()");
    }

    @AfterMethod
    void afterMethod(){
        System.out.println("@AfterMethod - Выполняюсь после метода test1()/test2()");
    }

    @AfterClass
    void afterClass(){
        System.out.println("@AfterClass - Выполняюсь после Выполняюсь перед тестовым классом Test1/Test2");
    }

    @AfterTest
    void afterTest(){
        System.out.println("@AfterTest - Выполняюсь после Test");
    }


    @AfterSuite
    void afterTestSuite(){
        System.out.println("@AfterSuite - Выполняюсь после Suite");
    }
}
