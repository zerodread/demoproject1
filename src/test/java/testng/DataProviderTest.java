package testng;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataProviderTest {
    String msg1 = "Happy ";
    String msg2 = "Automation!";

    @DataProvider
    public Object[][] data() {
        return new Object[][]{
                {msg1, msg2}
        };
    }

    @Test(dataProvider = "data")
    public void testingDataProvider(String message1, String message2) {
        System.out.println(message1+message2);
    }


}
